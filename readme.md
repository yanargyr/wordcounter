# Overview
This application is a command-line app that takes a path to a file as an argument. The output consists of a line for each word, with the number of its occurrences in the file. 
It will be sorted by the number of occurrences starting with the most frequent word.
 - Example:

foo: 24

bar: 17
...

The project is using Java 8 and Maven.

You can import the project in IntelliJ Idea.


# Build Guide
Install Java SDK 1.8 and Maven

Checkout project from git or extract zip file  

Open a command window, navigate to project root and execute
> mvn clean package

This will create 2 jar files

- wordcount-1.0-jar-with-dependencies.jar (standalone jar with dependencies included)
- wordcount-1.0.jar (jar with no dependencies, use this to import in other projects)


# Using the application 
## Executing Standalone package
Open a cmd window and navigate to the project root
Navigate to the 'target' folder
```bash 
cd target
```

- Option 1) Execute without arguments, type path when asked
```bash
java -jar wordcount-1.0-jar-with-dependencies.jar
```

- Option 2) Execute passing that path as argument 
```bash
java -jar wordcount-1.0-jar-with-dependencies.jar C:\Users\Yannis\Desktop\new19.txt
```
If path contains spaces, wrap path in double quotes
```bash
java -jar wordcount-1.0-jar-with-dependencies.jar "C:\Users\Yannis\Desktop\new 19.txt"
```
## Using the library

- Retrieve a String content of the file, wrap in a try-catch block
```java
try {
    final FileContentReader fileReader = new FileContentReader();
    String content = fileReader.getFileContent(path);   
} catch (IOException e) {
    ...
}
```

- Using a String content, retrieve a List of WordCounts objects, sorted by number their occurrences, in descending order.
```java
WordParser parser = new WordParser(content);
List<WordCounts> sortedWords = parser.getWordsSorted(false, WordCounts.BY_COUNT_ASC);
```

- Print the word occurrences
```java
for (WordCounts result : sortedWords) {
    System.out.println(result.toString());
}
```


# Assumptions
- The command line app will ignore case when it comes to identify unique words. 'Bar' and 'bar' will count as the same word.
However the library supports more options to match case or change order.

- If the command line demo app fails to load the file, it will ask for an alternative path. It will stop only after successfully parsing a file.

- The application will not try to remove any special characters. Words are separated by whitespace characters, space and break lines. Everything in between will be considered as a word. 


 