package wordcount.util;


import org.junit.jupiter.api.Test;
import wordcount.exceptions.WordCountsPathException;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class FileContentReaderTest {

    private static String PATH_1 = "src/test/resources/simple.txt";
    private static String BAD_PATH_1 = "A0:/blah/ pRogramZ and (x83) FiLEZ/1mpo$$ible PATH/myTexts/filenotfound.txt";
    private static String BAD_PATH_2 = "src/test/resources/file2.txt";
    private static String BAD_PATH_3 = "src/test/ASDASDA/mixedWords.txt";
    private static String BAD_PATH_4 = "src/test/resources";

    @Test
    public void canOpenFile() {
        final FileContentReader reader = new FileContentReader();
        try {
            String content = reader.getFileContent(PATH_1);
            assertEquals("foo bar foo bar", content);

        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void throwsIoExceptio_DirectoryNotFoundWithFullPath() {
        final FileContentReader reader = new FileContentReader();
        Exception exception = assertThrows(IOException.class,
                () -> reader.getFileContent(BAD_PATH_1));

        assertTrue(exception.getMessage().contains("The filename, directory name, or volume label syntax is incorrect"));
    }

    @Test
    public void throwsIoException_FileNotFoundButDirectoryExists() {
        final FileContentReader reader = new FileContentReader();
        Exception exception = assertThrows(IOException.class,
                () -> reader.getFileContent(BAD_PATH_2));

        assertTrue(exception.getMessage().contains("The system cannot find the file specified"));
    }

    @Test
    public void throwsIoException_DirectoryNotFoundWithRelativePath() {
        final FileContentReader reader = new FileContentReader();
        Exception exception = assertThrows(IOException.class,
                () -> reader.getFileContent(BAD_PATH_3));

        assertTrue(exception.getMessage().contains("The system cannot find the path specified"));
    }

    @Test
    public void throwsIoException_pathIsDirectory() {
        final FileContentReader reader = new FileContentReader();
        Exception exception = assertThrows(IOException.class,
                () -> reader.getFileContent(BAD_PATH_4));

        assertTrue(exception.getMessage().contains("Access is denied"));
    }

    @Test
    public void throwsWordCountsPathException_pathIsNull() {

        final FileContentReader reader = new FileContentReader();
        assertThrows(WordCountsPathException.class,
                () -> reader.getFileContent(null));
    }

    @Test
    public void throwsWordCountsPathException_pathIsEmpty() {
        final FileContentReader reader = new FileContentReader();
        assertThrows(WordCountsPathException.class,
                () -> reader.getFileContent(""));
    }
}
