package wordcount.util;

import org.junit.jupiter.api.Test;
import wordcount.model.WordCounts;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

public class WordParserTest {

    private static String PATH_1 = "src/test/resources/mixedWords.txt";
    private static String PATH_2 = "src/test/resources/wordsWithWhiteSpaces.txt";
    private static String PATH_3 = "src/test/resources/mixedWords2.txt";
    private static String PATH_4 = "src/test/resources/10000words.txt";

    @Test
    public void splitsWordsAndHandlesWhiteSpacesAndBreakLines() {
        String content = getContentFromFile(PATH_2);
        WordParser parser = new WordParser(content);

        List<WordCounts> sortedWords = parser.getWordsSorted(false, WordCounts.BY_COUNT_ASC);

        assertNotNull(sortedWords);

        //Should have found the word 'foobar' 9 times
        assertEquals(1, sortedWords.size());
        assertTrue(sortedWords.get(0).getCount() == 9L);
        assertEquals("foobar", sortedWords.get(0).getWord());
    }

    @Test
    public void countRightIgnoringCase() {
        String content = getContentFromFile(PATH_1);
        WordParser parser = new WordParser(content);

        List<WordCounts> sortedWords = parser.getWordsSorted(true, WordCounts.BY_COUNT_ASC);
        assertNotNull(sortedWords);
        // Expect to find exactly 8 words
        assertEquals(8, sortedWords.size());
    }

    @Test
    public void countRightMatchingCase() {
        String content = getContentFromFile(PATH_1);
        WordParser parser = new WordParser(content);

        List<WordCounts> sortedWords = parser.getWordsSorted(false, WordCounts.BY_COUNT_ASC);
        assertNotNull(sortedWords);

        // Expect to find exactly 10 words (foo and bar are both lower adn uppercase)
        assertEquals(10, sortedWords.size());
    }

    @Test
    public void sortByCountDescending() {
        String content = getContentFromFile(PATH_1);
        WordParser parser = new WordParser(content);

        List<WordCounts> sortedWords = parser.getWordsSorted(false, WordCounts.BY_COUNT_DESC);
        assertNotNull(sortedWords);

        // Every word's count should be >= the next one
        for (int i = 0; i < sortedWords.size() - 1; i++) {
            assertTrue(sortedWords.get(i).getCount() >= sortedWords.get(i + 1).getCount());
        }
    }

    @Test
    public void sortByCountAscending() {
        String content = getContentFromFile(PATH_1);
        WordParser parser = new WordParser(content);

        List<WordCounts> sortedWords = parser.getWordsSorted(true, WordCounts.BY_COUNT_ASC);
        assertNotNull(sortedWords);

        // Every word's count should be >= the next one
        for (int i = 0; i < sortedWords.size() - 1; i++) {
            assertTrue(sortedWords.get(i).getCount() <= sortedWords.get(i + 1).getCount());
        }
    }

    @Test
    public void sortAlphabeticallyDescending() {
        String content = getContentFromFile(PATH_1);
        WordParser parser = new WordParser(content);

        List<WordCounts> sortedWords = parser.getWordsSorted(true, WordCounts.BY_WORD_DESC);
        assertNotNull(sortedWords);

        assertEquals("yellow", sortedWords.get(0).getWord());

        // Every word is lexicographically greater than the next one
        for (int i = 0; i < sortedWords.size() - 1; i++) {
            assertTrue(sortedWords.get(i).getWord().compareTo(sortedWords.get(i + 1).getWord()) > 0);
        }
    }

    @Test
    public void sortAlphabeticallyAscending() {
        String content = getContentFromFile(PATH_1);
        WordParser parser = new WordParser(content);

        List<WordCounts> sortedWords = parser.getWordsSorted(true, WordCounts.BY_WORD_ASC);

        assertNotNull(sortedWords);

        assertEquals("bar", sortedWords.get(0).getWord());

        // Every word is lexicographically smaller than the next one
        for (int i = 0; i < sortedWords.size() - 1; i++) {
            assertTrue(sortedWords.get(i).getWord().compareTo(sortedWords.get(i + 1).getWord()) < 0);
        }
    }

    @Test
    public void sortsSameWayDifferetFilesWithSameWordsButDifferentOrder() {
        String content1 = getContentFromFile(PATH_1);
        WordParser parser1 = new WordParser(content1);

        List<WordCounts> sortedWords1 = parser1.getWordsSorted(true, WordCounts.BY_COUNT_DESC);
        assertNotNull(sortedWords1);

        String content2 = getContentFromFile(PATH_3);
        WordParser parser2 = new WordParser(content2);

        List<WordCounts> sortedWords2 = parser2.getWordsSorted(true, WordCounts.BY_COUNT_DESC);
        assertNotNull(sortedWords2);

        assertTrue(sortedWords2.size() == sortedWords1.size());

        // Both files should return the same results
        for (int i = 0; i < sortedWords1.size(); i++) {
            assertTrue(sortedWords1.get(i).getWord().equals(sortedWords2.get(i).getWord()));
            assertTrue(sortedWords1.get(i).getCount() == sortedWords2.get(i).getCount());
        }
    }

    @Test
    public void handlesFileWith10Kwords() {
        String content = getContentFromFile(PATH_4);
        WordParser parser = new WordParser(content);

        List<WordCounts> sortedWords = parser.getWordsSorted(true, WordCounts.BY_COUNT_DESC);

        assertNotNull(sortedWords);

        WordCounts word_aute = sortedWords.get(0); //192
        WordCounts word_reprehenderit = sortedWords.get(37); //157
        WordCounts word_fugiat = sortedWords.get(61); //127


        assertEquals(62, sortedWords.size());
        assertTrue(word_aute.getWord().equals("aute"));
        assertTrue(word_aute.getCount() == 192);

        assertTrue(word_reprehenderit.getWord().equals("reprehenderit"));
        assertTrue(word_reprehenderit.getCount() == 157);

        assertTrue(word_fugiat.getWord().equals("fugiat"));
        assertTrue(word_fugiat.getCount() == 127);
    }

    public static String getContentFromFile(String path) {
        try {
            return new FileContentReader().getFileContent(path);
        } catch (Exception e) {
            // This is really not supposed to happen
            return "";
        }
    }
}
