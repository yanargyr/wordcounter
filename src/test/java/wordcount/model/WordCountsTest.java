package wordcount.model;
import org.junit.jupiter.api.Test;
import wordcount.exceptions.WordCountsInvalidWord;
import wordcount.exceptions.WordCountsRangeException;

import static org.junit.jupiter.api.Assertions.*;

public class WordCountsTest {

    @Test
    public void createsObjectWithZeroOccurrences() {
        WordCounts entry = new WordCounts("test");
        assertEquals("test", entry.getWord());
        assertTrue(entry.getCount() == 0L);
    }

    @Test
    public void createsObjectWithFiveOccurrences() {
        WordCounts entry = new WordCounts("test", 5L);
        assertEquals("test", entry.getWord());
        assertTrue(entry.getCount() == 5L);
    }

    @Test
    public void throwsException_wordIsNull() {
        assertThrows(WordCountsInvalidWord.class,
                () -> new WordCounts(null));
    }

    @Test
    public void throwsException_wordIsEmpty() {
        assertThrows(WordCountsInvalidWord.class,
                () -> new WordCounts(""));
    }

    @Test
    public void throwsException_negativeOccurrences() {
        assertThrows(WordCountsRangeException.class,
                () -> new WordCounts("test", -1L));
    }

    @Test
    public void throwsException_nullOccurrences() {
        assertThrows(WordCountsRangeException.class,
                () -> new WordCounts("test", null));
    }
}
