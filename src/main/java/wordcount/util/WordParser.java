package wordcount.util;


import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import wordcount.model.WordCounts;

import java.util.*;
import java.util.stream.Collectors;


public class WordParser {

    private String content;

    public WordParser(final String content) {
        this.content = content;
    }



    /**
     * Parses the String content and returns a list of WordCount objects, sorted by given comparator
     *
     * @param ignoreCase if true, will ignore case
     * @param comparator a WordCounts comparator
     * @return a List<WordCounts> sorted
     */
    public List<WordCounts> getWordsSorted(boolean ignoreCase, Comparator<WordCounts> comparator) {
        Multiset<String> wordsSet = HashMultiset.create();

        // Add all words to map
        splitWords().forEach( word ->
                wordsSet.add(ignoreCase? word.toLowerCase() : word)
        );

        // Export to WordCounts and sort
        return wordsSet.elementSet().stream()
                .map(w -> new WordCounts(w, (long) wordsSet.count(w)))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    /**
     * Parses the content and extracts words
     *
     * @return A List<String> of all the words fount in the file content. It will contain duplicates
     */
    private List<String> splitWords() {
        // split by any whitespace character, not just spaces
        String regex = "\\s+";
        return Arrays.asList(this.content.split(regex));
    }
}
