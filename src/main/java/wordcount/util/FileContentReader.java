package wordcount.util;

import wordcount.exceptions.WordCountsPathException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileContentReader {


    /**
     * Reads a file given a path.
     *
     * @param path a full path or relative path to the file to scan
     * @return content of the file as String
     * @throws IOException, WordCountsPathException
     */
    public String getFileContent(String path) throws IOException, WordCountsPathException {

        if (path == null || path.length() == 0) {
            throw new WordCountsPathException(path);
        }

        StringBuilder fileContent = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(path));
        String strCurrentLine;

        while ((strCurrentLine = br.readLine()) != null) {
            // replacing break lines with space, else next word will concat
            if (fileContent.length() > 0) {
                fileContent.append(" ");
            }
            fileContent.append(strCurrentLine);
        }

        return fileContent.toString();
    }
}
