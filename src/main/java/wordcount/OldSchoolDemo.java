package wordcount;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


/**
 * One function execution without streams or maps, just simple arrays.
 * Made just for fun, use WordParserDemo instead
 */
public class OldSchoolDemo {

    public static void main(String[] args) {
        String path = null;
        String content = null;
        if (args.length == 1) {
            path = args[0];
            System.out.println("Reading file: " + path);
        } else {
            System.out.println("Path not specified.");
        }

        // Try to read the file
        while (content == null) {

            if (path == null) {
                Scanner scanner = new Scanner(System.in);
                System.out.println("Please type the full path to the file to parse:");
                path = scanner.next();
            }

            // read the file
            try {
                StringBuilder fileContent = new StringBuilder();
                BufferedReader br = new BufferedReader(new FileReader(path));
                String strCurrentLine;

                while ((strCurrentLine = br.readLine()) != null) {
                    // replacing break lines with space, else next word will concat
                    if (fileContent.length() > 0) {
                        fileContent.append(" ");
                    }
                    fileContent.append(strCurrentLine);
                }
                content = fileContent.toString();
            } catch (IOException e) {
                System.out.println("Could not read file");
                System.out.println(e.getMessage());
                content = null;
                path = null;
            }
        }

        // Split words
        String regex = "\\s+";
        List<String> allwords = Arrays.asList(content.split(regex));


        List<String> uniqueWords = new ArrayList<>();
        List<Long> uniqueWordsCounts = new ArrayList<>();

        // find all unique words
        for (String word : allwords) {
            boolean foundOne = false;
            for (int i = 0; i < uniqueWords.size(); i++) {
                String word2 = uniqueWords.get(i);

                if (word2.equalsIgnoreCase(word)) {
                    Long prevCount = uniqueWordsCounts.get(i);
                    uniqueWordsCounts.set(i, prevCount + 1);
                    foundOne = true;
                    break;
                }
            }
            if (!foundOne) {
                uniqueWords.add(word);
                uniqueWordsCounts.add(1L);
            }
        }

        // Sort unique words
        while (true) {
            boolean noSwaps = true;
            for (int i = 0; i < uniqueWords.size() - 1; i++) {
                int j = i + 1;
                if (uniqueWordsCounts.get(j) > uniqueWordsCounts.get(i)) {
                    Collections.swap(uniqueWords, i, j);
                    Collections.swap(uniqueWordsCounts, i, j);
                    noSwaps = false;
                }
            }
            if (noSwaps) break;
        }

        for (int i = 0; i < uniqueWords.size(); i++) {
            System.out.println(uniqueWords.get(i) + ": " + uniqueWordsCounts.get(i));
        }

        System.out.println("╔════════════════════════════════════════════════════════════════════════════════════════════════════════════╗");
        System.out.println("    _       ___   __ __    ___  _      __ __       ____  __ __  ____   ____   _      __ __   ");
        System.out.println("   | |     /   \\ |  |  |  /  _]| |    |  |  |     |    ||  |  ||    \\ |    \\ | |    |  |  |   ");
        System.out.println("   | |    |     ||  |  | /  [_ | |    |  |  |     |__  ||  |  ||  o  )|  o  )| |    |  |  |   ");
        System.out.println("   | |___ |  O  ||  |  ||    _]| |___ |  ~  |     __|  ||  |  ||     ||     || |___ |  ~  |   ");
        System.out.println("   |     ||     ||  :  ||   [_ |     ||___, |    /  |  ||  :  ||  O  ||  O  ||     ||___, |   ");
        System.out.println("   |     ||     | \\   / |     ||     ||     |    \\  `  ||     ||     ||     ||     ||     |   ");
        System.out.println("   |_____| \\___/   \\_/  |_____||_____||____/      \\____j \\__,_||_____||_____||_____||____/    ");
        System.out.println("                                                                                              ");
        System.out.println("╚════════════════════════════════════════════════════════════════════════════════════════════════════════════╝");
    }
}
