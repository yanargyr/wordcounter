package wordcount;

import wordcount.exceptions.WordCountsPathException;
import wordcount.model.WordCounts;
import wordcount.util.WordParser;
import wordcount.util.FileContentReader;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class WordCounterDemo {

    public static void main(String[] args) {
        String path;
        String content = null;
        if (args.length == 1) {
            path = args[0];
            System.out.println("Reading file: " + path);
            content = readFileFromUserInput(path);
        } else {
            System.out.println("Path not specified.");
        }

        // If file was not found, try getting user input
        while (content == null) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Please type the full path to the file to parse:");
            path = scanner.next();
            content = readFileFromUserInput(path);
        }

        WordParser parser = new WordParser(content);
        List<WordCounts> sortedWords = parser.getWordsSorted(true, WordCounts.BY_COUNT_DESC);

        for (WordCounts result : sortedWords) {
            System.out.println(result.toString());
        }
    }

    private static String readFileFromUserInput(final String path) {
        try {
            final FileContentReader fileReader = new FileContentReader();
            String content = fileReader.getFileContent(path);
            return content;

        } catch (IOException e) {
            System.out.println("Could not read file");
            System.out.println(e.getMessage());
            return null;
        } catch (WordCountsPathException e) {
            System.out.println("Could not read file");
            System.out.println(e.getMessage());
            return null;
        }
    }
}
