package wordcount.model;

import wordcount.exceptions.WordCountsInvalidWord;
import wordcount.exceptions.WordCountsRangeException;

import java.util.Comparator;


/**
 * An object that represents the number of occurrences of a word, inside a text
 */
public class WordCounts {
    /** The word */
    private String word;

    /** Number of occurrences */
    private Long count;

    public String getWord() {
        return word;
    }

    public Long getCount() {
        return count;
    }

    public WordCounts(final String word, final Long count) {
        if (word == null || word.length() == 0) {
            throw new WordCountsInvalidWord(word);
        }
        this.word = word;

        if (count == null || count < 0) {
            throw new WordCountsRangeException(count);
        }
        this.count = count;
    }

    public WordCounts(final String word) {
        if (word == null || word.length() == 0) {
            throw new WordCountsInvalidWord(word);
        }
        this.word = word;
        this.count = 0L;
    }


    @Override
    public String toString() {
        return word + ": " + count;
    }

    /**
     * Sorts by count ascending (less frequent words first)
      */
    public static final Comparator<WordCounts> BY_COUNT_ASC = Comparator.comparingLong(WordCounts::getCount);
    /**
     * Sorts by count descenting (most frequent words first)
     */
    public static final Comparator<WordCounts> BY_COUNT_DESC = Comparator.comparingLong(WordCounts::getCount).reversed();
    /**
     * Sorts alphabetically ascending ('a' before 'x')
     */
    public static final Comparator<WordCounts> BY_WORD_ASC = Comparator.comparing(WordCounts::getWord);
    /**
     * Sorts alphabetically descending ('x' before 'a')
     */
    public static final Comparator<WordCounts> BY_WORD_DESC = Comparator.comparing(WordCounts::getWord).reversed();
}
