package wordcount.exceptions;

public class WordCountsRangeException extends RuntimeException {

    public WordCountsRangeException(Long value) {
        super("Invalid number of occurrences: " + value);
    }
}
