package wordcount.exceptions;

public class WordCountsPathException extends Exception {

    public WordCountsPathException(final String path) {
        super("Invalid path provided: " + path);
    }
}
