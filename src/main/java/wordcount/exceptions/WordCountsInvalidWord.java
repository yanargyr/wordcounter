package wordcount.exceptions;

public class WordCountsInvalidWord extends RuntimeException {

    public WordCountsInvalidWord(final String word) {
        super("Invalid word:'" + word+"'");
    }
}
